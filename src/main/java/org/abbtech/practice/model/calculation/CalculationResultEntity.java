package org.abbtech.practice.model.calculation;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Entity
@Table(name = "calculation_result")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class CalculationResultEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(name = "a")
    BigDecimal a;
    @Column(name = "b")
    BigDecimal b;
    @Column(name = "result")
    BigDecimal calcResult;
    @Column(name = "method")
    String calcMethod;

    public CalculationResultEntity(BigDecimal a, BigDecimal b, BigDecimal calcResult, String calcMethod) {
        this.a = a;
        this.b = b;
        this.calcResult = calcResult;
        this.calcMethod = calcMethod;
    }
}

//package org.abbtech.practice.model.employee_manager;
//
//import jakarta.persistence.*;
//import lombok.*;
//
//import java.util.List;
//
//@Entity
//@Table(name = "employee", uniqueConstraints = {@UniqueConstraint(name = "uc_const_department_id", columnNames =
//        {"department_id"})})
//@AllArgsConstructor
//@NoArgsConstructor
//@Builder
//@Getter
//@Setter
//public class Employee {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//    @Column
//    private String name;
//
//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "department_id", referencedColumnName = "id")
//    private Department department;
//
//    @OneToOne(mappedBy = "employee", cascade = CascadeType.ALL)
//    private Address address;
//
//    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
//    @JoinTable(name = "employee_project", joinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"),
//        inverseJoinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"))
//    private List<Project> projects;
//}

package org.abbtech.practice.model.student_manager;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "Student")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "StudentID")
    private Long studentId;

    @Column(name = "FirstName")
    private String firstName;

    @Column(name = "LastName")
    private String lastName;

    @OneToOne
    @JoinColumn(name = "CardID", referencedColumnName = "CardID")
    private StudentIDCard studentIDCard;

    @ManyToMany
    @JoinTable(name = "StudentClass", joinColumns = @JoinColumn(name = "StudentID", referencedColumnName = "StudentID"),
            inverseJoinColumns = @JoinColumn(name = "ClassID", referencedColumnName = "ClassID"))
    private List<Class> classes;
}
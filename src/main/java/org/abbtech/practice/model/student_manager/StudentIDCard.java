package org.abbtech.practice.model.student_manager;

import jakarta.persistence.*;
import lombok.*;

import java.sql.Date;

@Entity
@Table(name = "StudentIDCard")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class StudentIDCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CardID")
    private Long cardId;

    @Column(name = "CardNumber")
    private String cardNumber;

    @Column(name = "ExpiryDate")
    private Date expiryDate;

    @OneToOne(mappedBy = "studentIDCard")
    private Student student;

    @Builder
    public StudentIDCard(String cardNumber, Date expiryDate) {
        this.cardNumber = cardNumber;
        this.expiryDate = expiryDate;
    }
}
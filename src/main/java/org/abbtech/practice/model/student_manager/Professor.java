package org.abbtech.practice.model.student_manager;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "Professor")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ProfessorID")
    private Long professorId;

    @Column(name = "FirstName")
    private String firstName;

    @Column(name = "LastName")
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "DepartmentID", referencedColumnName = "DepartmentID")
    private Department department;
}
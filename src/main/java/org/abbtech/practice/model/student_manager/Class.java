package org.abbtech.practice.model.student_manager;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "Class")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Class {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ClassID")
    private Long classId;

    @Column(name = "ClassName")
    private String className;

    @ManyToMany(mappedBy = "classes")
    private List<Student> students;
}
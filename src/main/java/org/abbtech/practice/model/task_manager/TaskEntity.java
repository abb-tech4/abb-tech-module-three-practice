package org.abbtech.practice.model.task_manager;

public class TaskEntity {
    private Long id;
    private String description;

    public TaskEntity(String description) { // considering id is serial
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "[Id: " + this.getId() + "; Description: " + this.getDescription() + "]";
    }
}

package org.abbtech.practice.controller;

import org.abbtech.practice.dto.TaskDTO;
import org.abbtech.practice.model.task_manager.TaskEntity;
import org.abbtech.practice.repository.TaskRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/taskController")
public class TaskController {
    TaskRepository taskRepository;

    public TaskController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @GetMapping("/tasks")
    public List<TaskDTO> get() {
        List<TaskEntity> taskEntities = taskRepository.getTasks();
        List<TaskDTO> result = new ArrayList<>();

        for (TaskEntity t : taskEntities) {
            result.add(new TaskDTO(t.getId(), t.getDescription()));
        }

        return result;
    }

    @GetMapping("/tasksById")
    public List<TaskDTO> getByIds(@RequestParam List<Long> ids) {
        List<TaskEntity> taskEntities = taskRepository.getTasksById(ids);
        List<TaskDTO> result = new ArrayList<>();

        for (TaskEntity t : taskEntities) {
            result.add(new TaskDTO(t.getId(), t.getDescription()));
        }

        return result;
    }

    @PutMapping("/updateTask/{id}")
    public TaskDTO updateTask(@PathVariable Long id, @RequestBody TaskDTO taskDTO) {
        TaskEntity task = new TaskEntity(taskDTO.description());
        task.setId(id);
        taskRepository.updateTask(task);
        return taskDTO;
    }

    @PostMapping("/saveTask")
    public TaskDTO saveTask(@RequestBody TaskDTO taskDTO) {
        TaskEntity task = new TaskEntity(taskDTO.description());
        taskRepository.saveTask(task);
        return taskDTO;
    }

    @PostMapping("/createTasks")
    public List<TaskDTO> createTasks(@RequestBody List<TaskDTO> taskDTOs) {
        List<TaskEntity> taskEntities = new ArrayList<>();
        for (TaskDTO t : taskDTOs) {
            taskEntities.add(new TaskEntity(t.description()));
        }
        taskRepository.createTasks(taskEntities);
        return taskDTOs;
    }

    @DeleteMapping("/deleteTasks")
    public ResponseEntity<String> deleteTasks(@RequestBody List<TaskDTO> taskDTOs) {
        List<TaskEntity> taskEntities = new ArrayList<>();
        for (int i = 0; i < taskDTOs.size(); i++) {
            taskEntities.add(new TaskEntity(taskDTOs.get(i).description()));
            taskEntities.get(i).setId(taskDTOs.get(i).id());
        }

        taskRepository.deleteTasks(taskEntities);

        return ResponseEntity.ok("Tasks deleted successfully.");
    }
}

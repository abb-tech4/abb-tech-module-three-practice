package org.abbtech.practice.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.abbtech.practice.dto.StudentIDCardDTO;
import org.abbtech.practice.model.student_manager.Student;
import org.abbtech.practice.model.student_manager.StudentIDCard;
import org.abbtech.practice.repository.StudentCardRepository;
import org.abbtech.practice.repository.StudentRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class StudentManagerController {
    StudentRepository studentRepository;
    StudentCardRepository studentCardRepository;

    @PostMapping("/saveStudent")
    public void addStudent(@RequestBody StudentIDCardDTO studentIDCardDTO) {
        StudentIDCard newCard = new StudentIDCard();
        newCard.setCardNumber(studentIDCardDTO.cardNumber());
        newCard.setExpiryDate(convertStringToSqlDate(studentIDCardDTO.expiryDate()));
        studentCardRepository.save(newCard);

        Student newStudent = new Student();
        newStudent.setFirstName("Name");
        newStudent.setLastName("Surname");
        newStudent.setStudentIDCard(newCard);
        studentRepository.save(newStudent);
    }

    public static Date convertStringToSqlDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(dateString, formatter);

        return Date.valueOf(date);
    }
}
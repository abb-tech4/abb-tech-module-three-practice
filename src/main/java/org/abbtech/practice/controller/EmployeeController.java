//package org.abbtech.practice.controller;
//
//import lombok.RequiredArgsConstructor;
//import org.abbtech.practice.dto.EmployeeDTO;
//import org.abbtech.practice.dto.XUserGroup;
//import org.abbtech.practice.dto.YUserGroup;
//import org.abbtech.practice.service.EmployeeOperationServiceImpl;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@RequestMapping("/employees")
//@RequiredArgsConstructor
//@Validated
//public class EmployeeController {
//    private final EmployeeOperationServiceImpl employeeOperationService;
//
//    @PostMapping
//    public void saveEmployee(@RequestBody @Validated(value = YUserGroup.class) EmployeeDTO employee) {
//        employeeOperationService.save(employee);
//    }
//
//    @PostMapping("/custom")
//    public void saveEmployeeValidated(@RequestBody @Validated(value = XUserGroup.class) EmployeeDTO employee) {
//        employeeOperationService.save(employee);
//    }
//
//    @GetMapping
//    public List<EmployeeDTO> getAllEmployees() {
//        return employeeOperationService.getAllEmployees();
//    }
//}
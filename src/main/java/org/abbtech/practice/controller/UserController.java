package org.abbtech.practice.controller;

import jakarta.validation.Valid;
import org.abbtech.practice.dto.user_manager.PostDTO;
import org.abbtech.practice.dto.user_manager.UserDTO;
import org.abbtech.practice.service.UserOperationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/userController")
@Validated
public class UserController {
    UserOperationServiceImpl userService;

    @Autowired
    public UserController(UserOperationServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping("/saveUser")
    public ResponseEntity<String> addUser(@RequestBody @Valid UserDTO userDTO) {
        return userService.createUser(userDTO);
    }

    @PostMapping("/addPost")
    public ResponseEntity<String> addPost(@RequestBody @Valid PostDTO postDTO, @RequestParam Long userId) {
        return userService.addPostWithComments(userId, postDTO);
    }

    @GetMapping("/posts")
    public ResponseEntity<List<PostDTO>> getPosts(@RequestParam Long userId, @RequestParam Long postId) {
        return userService.getPostsByUserIdAndPostId(userId, postId);
    }
}

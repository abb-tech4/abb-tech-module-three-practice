package org.abbtech.practice.service;

import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.user_manager.BatchCommentsDTO;
import org.abbtech.practice.dto.user_manager.CommentDTO;
import org.abbtech.practice.dto.user_manager.PostDTO;
import org.abbtech.practice.dto.user_manager.UserDTO;
import org.abbtech.practice.model.user_manager.Comment;
import org.abbtech.practice.model.user_manager.Post;
import org.abbtech.practice.model.user_manager.User;
import org.abbtech.practice.repository.PostRepository;
import org.abbtech.practice.repository.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Validated
public class UserOperationServiceImpl implements UserOperationService {
    private final UserRepository userRepository;
    private final PostRepository postRepository;

    @Override
    @Transactional
    public ResponseEntity<String> createUser(@Valid UserDTO userDTO) {
        // Create user without posts and comments initially
        User user = User.builder()
                .username(userDTO.username())
                .email(userDTO.email())
                .password(userDTO.password())
                .build();

        // Handle posts and their comments
        if (userDTO.posts() != null) {
            user.setPosts(userDTO.posts().stream()
                    .map(postDTO -> {
                        Post post = Post.builder()
                                .title(postDTO.title())
                                .content(postDTO.content())
                                .createdDate(Timestamp.from(Instant.now()))
                                .user(user)
                                .build();

                        // Handle comments on the post, which may not be authored by the user creating the post
                        List<Comment> comments = postDTO.comments().comments().stream()
                                .map(commentDTO -> Comment.builder()
                                        .content(commentDTO.content())
                                        .createdDate(Timestamp.from(Instant.now()))
                                        .post(post)
                                        .build()).toList();
                        post.setComments(comments);
                        return post;
                    }).toList());
        }

        userRepository.save(user);
        return ResponseEntity.ok("User created successfully with ID: " + user.getId());
    }

    @Override
    @Transactional
    public ResponseEntity<String> addPostWithComments(Long userId, @Valid PostDTO postDTO) {
        // Check if the user exists
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isEmpty()) {
            return ResponseEntity.badRequest().body("User with ID " + userId + " does not exist.");
        }
        User user = userOptional.get();

        // Create the post
        Post post = Post.builder()
                .title(postDTO.title())
                .content(postDTO.content())
                .createdDate(Timestamp.from(Instant.now()))
                .user(user)
                .build();

        // Map comments if any
        if (postDTO.comments() != null) {
            List<Comment> comments = postDTO.comments().comments().stream()
                    .map(commentDTO -> Comment.builder()
                            .content(commentDTO.content())
                            .createdDate(Timestamp.from(Instant.now()))
                            .post(post)
                            .build()).toList();
            post.setComments(comments);
        }

        postRepository.save(post);
        return ResponseEntity.ok("Post with comments added successfully for User ID: " + userId);
    }

    public ResponseEntity<List<PostDTO>> getPostsByUserIdAndPostId(Long userId, Long postId) {
        Post post = postRepository.findById(postId).orElse(null);
        if (post == null) {
            return ResponseEntity.notFound().build();
        }

        if (!post.getUser().getId().equals(userId)) {
            return ResponseEntity.badRequest().body(Collections.emptyList());
        }

        PostDTO postDTO = convertToPostDTO(post);
        return ResponseEntity.ok(Collections.singletonList(postDTO));
    }

    private PostDTO convertToPostDTO(Post post) {
        List<CommentDTO> commentDTOs = post.getComments().stream()
                .map(comment -> new CommentDTO(
                        comment.getContent(),
                        comment.getCreatedDate(),
                        comment.getPost().getId()
                )).toList();

        @Valid
        BatchCommentsDTO batchCommentsDTO = new BatchCommentsDTO(commentDTOs);

        return new PostDTO(
                post.getTitle(),
                post.getContent(),
                post.getCreatedDate(),
                post.getUser().getId(),
                batchCommentsDTO
        );
    }
}

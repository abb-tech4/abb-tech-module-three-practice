package org.abbtech.practice.service;

import jakarta.validation.Valid;
import org.abbtech.practice.dto.user_manager.PostDTO;
import org.abbtech.practice.dto.user_manager.UserDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface UserOperationService {
    ResponseEntity<String> createUser(@Valid UserDTO user);
    ResponseEntity<String> addPostWithComments(Long userId, @Valid PostDTO postDTO);
    ResponseEntity<List<PostDTO>> getPostsByUserIdAndPostId(Long userId, Long postId);
}

//package org.abbtech.practice.service;
//
//import lombok.RequiredArgsConstructor;
//import org.abbtech.practice.dto.EmployeeDTO;
//import org.abbtech.practice.dto.ProjecDTO;
//import org.abbtech.practice.model.employee_manager.Department;
//import org.abbtech.practice.model.employee_manager.Employee;
//import org.abbtech.practice.model.employee_manager.Project;
//import org.abbtech.practice.repository.EmployeeRepository;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Service
//@RequiredArgsConstructor
//public class EmployeeOperationServiceImpl implements EmployeeOperationService {
//
//    private final EmployeeRepository employeeRepository;
//
//    @Override
//    public void save(EmployeeDTO employee) {
//        employeeRepository.save(Employee.builder()
//                .name(employee.name())
//                .projects(employee.projects().stream()
//                        .map(projecReqtDTO -> Project
//                                .builder()
//                                .name(projecReqtDTO.name())
//                                .build()).toList())
//                .department(Department.builder()
//                        .name(employee.department())
//                        .build())
//                .build());
//    }
//
//    @Override
//    @Transactional
//    public List<EmployeeDTO> getAllEmployees() {
//        var employees = employeeRepository.findAll();
//        return employees.stream()
//                .map(employee -> new EmployeeDTO(employee.getName(),
//                        employee.getDepartment().getName(),
//                        employee.getProjects()
//                                .stream().map(project -> new ProjecDTO(
//                                        project.getName()))
//                                .toList(), 4))
//                .toList();
//    }
//}
package org.abbtech.practice.repository.impl;

import org.abbtech.practice.model.task_manager.TaskEntity;
import org.abbtech.practice.repository.TaskRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class TaskRepositoryImpl implements TaskRepository {
    private JdbcTemplate jdbcTemplate;

    public TaskRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<TaskEntity> getTasks() {
        String sql = "SELECT * FROM tasks";
        return jdbcTemplate.query(
                sql,
                (rs, rowNum) -> {
                    TaskEntity task = new TaskEntity(rs.getString("description"));
                    task.setId(rs.getLong("id"));
                    return task;
                }
        );
    }


    @Override
    public List<TaskEntity> getTasksById(List<Long> ids) {
        if (ids.isEmpty()) {
            return Collections.emptyList();
        }

        String sql = "SELECT * FROM tasks WHERE id IN (" +
                String.join(",", Collections.nCopies(ids.size(), "?")) + ")";

        return jdbcTemplate.query(
                sql,
                (rs, rowNum) -> {
                    TaskEntity task = new TaskEntity(rs.getString("description"));
                    task.setId(rs.getLong("id"));
                    return task;
                },
                ids.toArray()
        );
    }

    @Override
    public void saveTask(TaskEntity task) {
        jdbcTemplate.update("""
                INSERT INTO tasks(description) VALUES (?);
                """, task.getDescription());
    }

    @Override
    public void updateTask(TaskEntity task) {
        jdbcTemplate.update("""
                UPDATE tasks SET description= ? WHERE id = ?
                """, task.getDescription(), task.getId());
    }

    @Override
    public void createTasks(List<TaskEntity> tasks) {
        for (TaskEntity task : tasks) {
            saveTask(task);
        }
    }

    @Override
    public void deleteTasks(List<TaskEntity> tasks) {
        String sql = "DELETE FROM tasks WHERE id = ?";

        List<Object[]> ids = tasks.stream()
                .map(task -> new Object[] { task.getId() })
                .toList();

        jdbcTemplate.batchUpdate(sql, ids);
    }
}
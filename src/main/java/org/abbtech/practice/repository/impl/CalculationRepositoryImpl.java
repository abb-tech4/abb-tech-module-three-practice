package org.abbtech.practice.repository.impl;

import org.abbtech.practice.model.calculation.CalculationResultEntity;
import org.abbtech.practice.repository.CalculationRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class CalculationRepositoryImpl implements CalculationRepository {
    private JdbcTemplate jdbcTemplate;
    @Value("${custom.property.name}")
    public String customPropertyName;
    @Value("${custom.property.value}")
    public Integer customPropertyValue;

    public CalculationRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insert(CalculationResultEntity calculationResultEntity) {
        jdbcTemplate.update("""
                INSERT INTO CALCULATION_RESULT(a, b, result, method) VALUES (?,?,?,?);
                """, calculationResultEntity.getA(), calculationResultEntity.getB(), calculationResultEntity.getCalcResult(),
                calculationResultEntity.getCalcMethod());
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insertBatch(List<CalculationResultEntity> calculationResultEntities) {
        jdbcTemplate.batchUpdate("""
                INSERT INTO CALCULATION_RESULT(a, b, result, method) VALUES (?,?,?,?);
                """, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setBigDecimal(1, calculationResultEntities.get(i).getA());
                ps.setBigDecimal(2, calculationResultEntities.get(i).getB());
                ps.setBigDecimal(3, calculationResultEntities.get(i).getCalcResult());
                ps.setString(4, calculationResultEntities.get(i).getCalcMethod());
            }

            @Override
            public int getBatchSize() {
                return calculationResultEntities.size();
            }
        });
    }

    @Override
    public void update(CalculationResultEntity calculationResultEntity) {
//        jdbcTemplate.update("""
//                UPDATE CALCULATION_RESULT SET a = ?, b, result, method) VALUES (?,?,?,?);
//                """, calculationResultEntity.getA(), calculationResultEntity.getB(), calculationResultEntity.getResult(),
//                calculationResultEntity.getMethod());
    }

    @Override
    public Optional<CalculationResultEntity> findById(Long id) {

        return jdbcTemplate.queryForObject("""
                SELECT * FROM calculation_result WHERE id = ?
                """, (rs, rowNum) -> Optional.ofNullable(new CalculationResultEntity(rs.getBigDecimal("a"),
                rs.getBigDecimal("b"),
                rs.getBigDecimal("result"),
                rs.getString("method"))), id);
    }

    @Override
    public void delete(Long id) {

    }
}

package org.abbtech.practice.repository;

import org.abbtech.practice.model.calculation.CalculationResultEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CalculationRepositorySpringData extends JpaRepository<CalculationResultEntity, Long> {
    List<CalculationResultEntity> findAllByCalcMethodEqualsIgnoreCase(String calcMethod);

    @Query(value = "FROM CalculationResultEntity ce WHERE ce.calcMethod =: param")
    List<CalculationResultEntity> findAllCustom(@Param("param") String calcMethod);
}
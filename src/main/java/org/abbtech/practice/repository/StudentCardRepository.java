package org.abbtech.practice.repository;

import org.abbtech.practice.model.student_manager.StudentIDCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentCardRepository extends JpaRepository<StudentIDCard, Long> {
}

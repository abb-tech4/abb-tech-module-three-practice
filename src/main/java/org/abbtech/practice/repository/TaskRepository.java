package org.abbtech.practice.repository;

import org.abbtech.practice.model.task_manager.TaskEntity;

import java.util.List;

// Get all tasks, getTasksById, saveTask,updateTask, createTasks(Batch),deleteTasks
public interface TaskRepository {
    List<TaskEntity> getTasks();
    List<TaskEntity> getTasksById(List<Long> ids);
    void saveTask(TaskEntity task);
    void updateTask(TaskEntity task);
    void createTasks(List<TaskEntity> tasks);
    void deleteTasks(List<TaskEntity> tasks);
}
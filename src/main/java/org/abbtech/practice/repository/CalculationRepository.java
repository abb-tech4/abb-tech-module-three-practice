package org.abbtech.practice.repository;

import org.abbtech.practice.model.calculation.CalculationResultEntity;

import java.util.List;
import java.util.Optional;

public interface CalculationRepository {
    void insert(CalculationResultEntity calculationResultEntity);
    void insertBatch(List<CalculationResultEntity> calculationResultEntities);
    void update(CalculationResultEntity calculationResultEntity);
    Optional<CalculationResultEntity> findById(Long id);
    void delete(Long id);
}

package org.abbtech.practice.bean;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaBeanWithConfiguration {

    @Bean("Student1")
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public Student getStudent() {
        Student student = new Student();
        student.name = "Value";
        return student;
    }

    @Bean("Student12")
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Student getStudent1() {
        Student student = new Student();
        student.name = "Value1";
        return student;
    }
}
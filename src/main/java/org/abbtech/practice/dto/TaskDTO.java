package org.abbtech.practice.dto;

public record TaskDTO(Long id, String description) {
}

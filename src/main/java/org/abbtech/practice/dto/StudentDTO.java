package org.abbtech.practice.dto;

import java.util.Map;

public record StudentDTO(String name, String surname, Integer age, Map<String, String> variables) {
}
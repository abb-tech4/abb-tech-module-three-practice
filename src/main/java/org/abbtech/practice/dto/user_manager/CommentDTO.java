package org.abbtech.practice.dto.user_manager;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

import java.sql.Timestamp;

public record CommentDTO(
        @NotEmpty(message = "Content cannot be empty") @Size(min = 5, max = 100, message = "Content must be between 5 and 100 characters")
        String content,
        Timestamp createdDate,
        Long postId) {
}
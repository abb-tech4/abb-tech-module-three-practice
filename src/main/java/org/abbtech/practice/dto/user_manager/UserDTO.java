package org.abbtech.practice.dto.user_manager;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.util.List;

public record UserDTO(
        @NotBlank(message = "not blank username") @Size(min = 5, max = 15, message = "The length of the username can be in the range [5;15]")
        String username,
        @Pattern(regexp = "^[a-zA-Z\\d+_.-]+@[a-zA-Z\\d.-]+$", message = "Please enter a valid email address.")
        String email,
        @NotBlank @Size(min = 10, max = 15, message = "The password length should be in the range [10;15]")
        String password,
        List<PostDTO> posts) {
}
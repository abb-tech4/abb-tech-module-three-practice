package org.abbtech.practice.dto.user_manager;

import jakarta.validation.constraints.Size;

import java.util.List;

public record BatchCommentsDTO(
        @Size(min = 2, max = 5, message = "The number of comments is minimum 2 and maximum 5")
        List<CommentDTO> comments) {
}
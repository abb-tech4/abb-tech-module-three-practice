package org.abbtech.practice.dto.user_manager;

import jakarta.validation.constraints.Size;

import java.sql.Timestamp;

public record PostDTO(@Size(min = 3, max = 20, message = "Title should be in the range [3;20]") String title,
                      @Size(min = 30, max = 150, message = "Content should be in the range [30;150]") String content,
                      Timestamp createdDate,
                      Long userId,
                      BatchCommentsDTO comments) {
}
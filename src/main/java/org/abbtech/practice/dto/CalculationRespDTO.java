package org.abbtech.practice.dto;

import java.math.BigDecimal;

public record CalculationRespDTO(BigDecimal result) {
}
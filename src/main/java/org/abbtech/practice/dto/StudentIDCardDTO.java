package org.abbtech.practice.dto;

public record StudentIDCardDTO(String cardNumber, String expiryDate) {
}
